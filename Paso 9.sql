show databases;
create database store;
drop database store;
use  store;

create table cliente(
	codCliente int auto_increment primary key,
    nombre varchar(50),
    apellidos varchar(50),
    empresa varchar(50),
    puesto varchar(50),
    direccion varchar(50),
    poblacion varchar(25) default 'Écija',
    cp varchar(5) default  '41400',
    provincia varchar(25) default 'Sevilla',
    telefono varchar(9),
    fechaNacimiento date
);

show tables;
describe cliente;

create table articulo(
	codArticulo int auto_increment primary key,
    nombre varchar(50) not null,
    descripcion varchar(50) not null,
    precioUnidad float(5,2) unsigned not null,
    unidadStock int,
    stockSeguridad int,
    imagen varchar(50)
);
describe articulo;

create table compra(
	codCompra int auto_increment,
	codCliente int not null,
    codArticulo int not null,
    fecha date not null,
    unidad int not null,
    primary key (codCompra,codCliente, codArticulo),
    constraint fk_cliente_compra foreign key (codCliente) references cliente(codCliente),
    constraint fk_articulo_compra foreign key (codArticulo) references articulo(codArticulo)
);
describe compra;



insert into cliente (nombre,apellidos,empresa, puesto,direccion,telefono,fechaNacimiento) values
('José', 'Fernández Ruiz', 'Estudio Cero', 'Gerente','Cervantes,13','656789043','1968-06-13'),
('Luis', 'Fernández Chacón', 'Beep', 'Dependiente','Aurora, 4','675894566','1982-05-24'),
('Antonio', 'Ruiz Gómez', 'Comar', 'Dependiente', 'Osuna, 23', '654345544', '1989-08-06'),
('Andrea', 'Romero Vázquez', 'Estudio Cero', 'Dependiente', 'Cervantes, 25', '646765657', '1974-11-23'),
('José', 'Pérez Pérez', 'Beep', 'Gerente', 'Córdoba, 10', '645345543', '1978-04-10');

insert into cliente (nombre,empresa)  values ('Laura', 'Beep');

select * from cliente;

insert into articulo (nombre,descripcion,precioUnidad,unidadStock, stockSeguridad) values
('NETGEAR switch prosafe', 'Switch 8 puertos GigabitEthernet', 125, 3, 2),
('Switch SRW224G4-EU de Linksys', 'CISCO switch 24 puertos 10/100', 202.43, 2, 2),
('Switch D-link', 'D-Link smart switch 16 puertos', 149.90, 7, 4),
('Switch D-link', 'D-Link smart switch 48 puertos', 489.00, 4, 2);

select * from articulo;


insert into compra (codCliente,codArticulo,fecha,unidad) values
(1,1,'2010-10-13',2),
(1,2,'2010-10-13',1),
(2,3,'2010-10-15',1),
(2,4,'2010-10-15',1),
(3,1,'2010-10-15',2),
(4,2,'2010-10-15',1),
(5,3,'2010-10-15',3),
(1,4,'2010-10-16',1),
(1,1,'2010-10-16',2),
(2,2,'2010-10-17',1),
(3,3,'2010-10-18',4),
(4,4,'2010-10-19',2),
(5,1,'2010-10-19',1);

select * from compra;


-- ----------------------------- Consultas:

-- 1. Mostrar los apellidos y teléfono de los clientes llamados José o Luis ordenados alfabéticamente por nombres.
select nombre,apellidos, telefono from cliente where nombre like '%José%' or nombre like '%Luis%' order by nombre;

-- 2. Mostrar todos los clientes ordenados por su fecha de nacimiento.
select * from cliente order by fechaNacimiento;

-- 3. Mostrar nombre y apellidos de los clientes que no tengan teléfono.
select nombre,apellidos from cliente where telefono is null;

-- 4. Mostrar aquellos productos cuyo stock en almacén sea menor que cuatro.
select nombre as Nombre, descripcion as Descripcion, unidadStock as Stock_en_Almacen from articulo where unidadStock < 4;

-- 5. Mostrar el nombre, la descripción y la ruta de la imagen de los productos que valgan menos de 200 €.
select nombre as Nombre,descripcion as Descripcion,imagen as 'Ruta de imagen', concat(precioUnidad, ' €') as Precio_Unidad from articulo where precioUnidad < 200;

-- 6. Descubrir cuántos productos tenemos de D-link.
select nombre as Nombre, count(*) as Cantidad from articulo where nombre like '%D-link%';

-- 7. ¿Cuál es la descripción de los productos de D-link?
select descripcion as Descripcion from articulo where nombre like '%D-link%';

-- 8. ¿Cuántas compras se llevan realizadas?
select count(codCliente) as Total_Compra from compra;

-- 9. ¿Cuántas compras ha realizado el cliente 4? ¿Quién es?
-- Count devuelve una fila
select count(compra.codCliente) as 'Número de compras', cliente.nombre from compra, cliente where compra.codCliente = cliente.codCliente and compra.codCliente = 4;
select cliente.nombre as Cliente, count(*) as Numero_Compras from compra inner join cliente on cliente.codCliente = compra.codCliente where compra.codCliente = 4;

-- 10.  ¿Cuántas unidades se han comprado del producto 2? ¿Qué producto es?
select sum(unidad) from compra where codArticulo = 2;
select articulo.nombre as Producto, sum(compra.unidad) as Unidades from compra inner join articulo on articulo.codArticulo = compra.codArticulo where compra.codArticulo = 2;


-- 11. ¿Cuál es el precio más caro de la tienda? ¿A qué producto pertenece?
select nombre, concat(max(precioUnidad), ' €') as Mayor_Precio from articulo;

-- 12. ¿Qué media de precios se maneja en la tienda? ¿Cuántos de los actuales productos superan ese precio medio? ¿Qué productos son?
select  concat(truncate(avg(precioUnidad),2), ' €') as Precio_Medio, count(precioUnidad), nombre from articulo where  precioUnidad > (select avg(precioUnidad) from articulo);

-- 13. Mostrar, sin repeticiones, los nombres de todos nuestros clientes.
select distinct nombre from cliente;
select nombre from cliente group by nombre;

-- 14. Obtener el nombre y el teléfono de los clientes cuya edad está comprendida entre 30 y 40 años ordenados por edad.
select nombre,telefono,year(now()) - year(fechaNacimiento) as Edad  from cliente;
select nombre,telefono,year(now()) - year(fechaNacimiento) as Edad  from cliente where year(now()) - year(fechaNacimiento) between 30 and 40 order by year(now()) - year(fechaNacimiento);


-- 15. Devuelve la fecha más antigua
select min(fechaNacimiento) from cliente;